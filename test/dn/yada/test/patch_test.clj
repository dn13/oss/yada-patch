(ns dn.yada.test.patch-test
  (:require [midje.sweet :refer :all]
            [dn.yada.patch]
            [yada.yada :as yada]))
 
(def patch-resource
  (-> {:methods {:patch {:response (constantly {:response :test})}}}
      (assoc :consumes "*/*"
             :produces "application/edn")
      (yada/resource)))

(def routes ["/patch" patch-resource])

(facts "patch route"
    (fact "returns valid response"
      (yada/response-for routes :patch "/patch") => (contains {:headers anything
                                                               :status 200
                                                               :body #"\{:response :test\}\W*"})))
