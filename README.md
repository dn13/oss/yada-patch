# Yada Patch

By default [Yada](https://github.com/juxt/yada) does not support HTTP PATCH, but it's not
that hard to add support for it.  That's exactly what this mini-lib does.

## Usage

Add a reference to your dependencies:

```clojure
[dn/yada-patch "1.4.0-alpha1"]  ;; Version number matches the Yada version
```

And to use a resource, require it in your file:

```clojure
(require '[dn.yada.patch :refer :all])

(def my-resource
  (yada/resource
    {:methods {:patch {:response #'my-handler}}}))
```

This should allow you to use `PATCH` requests in your application.

As of version `1.4.0` the Yada dependency has `:scope :provided`, which
means you have to provide your own Yada dependency in your project.
This avoids collisions with other versions.

## License

Copyright © 2020-2021 by [Debreuck Neirynck](https://www.d-n.be)

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
