(defproject dn/yada-patch "1.4.0-SNAPSHOT"
  :description "Adds PATCH verb to Yada"
  :url "https://gitlab.com/dn13/oss/yada-patch"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.2"]
                 [yada "1.4.0-alpha1" :scope "provided"]
                 [javax.xml.bind/jaxb-api "2.3.1"]]
  :repl-options {:init-ns dn.yada.patch}
  :profiles {:dev
             {:dependencies [[midje "1.10.2"]
                             [dn/midje-junit-formatter "0.1.1"]
                             [yada "1.4.0-alpha1"]]
              :plugins [[lein-midje "3.2.2"]]}}
  :deploy-repositories
  [["snapshots" {:url "https://nexus.d-n.be/repository/maven-snapshots"
                 :username [:gpg :env/nexus_user]
                 :password [:gpg :env/nexus_pass]
                 :snapshots true}]
   ["releases"  {:url "https://clojars.org/repo"
                 :snapshots false
                 :username [:gpg :env/clojars_user]
                 :password [:gpg :env/clojars_pass]}]])
