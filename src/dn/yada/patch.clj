(ns dn.yada.patch
  "Implementation for PATCH, which is not supported by default by Yada"
  (:require [manifold.deferred :as d]
            [yada
             [methods :as m]
             [util :as util]])
  (:import yada.context.Response))

;; This is mostly copied from POST in yada/methods.clj and modified

(defn- zero-content-length
  "Unless status code is 1xx or 204, or method is CONNECT. We don't set
  the content-length in the case of a 304. Ensure status is set
  in [:response :status] prior to call this, and only call if
  response :body is nil. See rfc7230#section-3.3.2 for specification
  details."
  [ctx]
  (let [status (-> ctx :response :status)]
    (assert status)
    (or
     (when (and
            (nil? (get-in ctx [:response :headers "content-length"]))
            (not= status 304)
            (>= status 200)
            (not= (:method ctx) :connect))
       (assoc-in ctx [:response :headers "content-length"] 0))
     ctx)))

(defprotocol PatchResult
  (interpret-patch-result [_ ctx]))

(extend-protocol PatchResult
  Response
  (interpret-patch-result [response ctx]
    (assoc ctx :response response))
  Boolean
  (interpret-patch-result [b ctx]
    (if b ctx (throw (ex-info "Failed to process PATCH" {}))))
  Object
  (interpret-patch-result [o ctx]
    (assoc-in ctx [:response :body] o))
  clojure.lang.Fn
  (interpret-patch-result [f ctx]
    (interpret-patch-result (f ctx) ctx))
  java.net.URI
  (interpret-patch-result [uri ctx]
    (-> ctx
        (assoc-in [:response :status]
                  (if (util/same-origin? (:request ctx)) 303 201))
        (assoc-in [:response :headers "location"]
                  (str uri))))

  java.net.URL
  (interpret-patch-result [url ctx]
    (interpret-patch-result (.toURI url) ctx))

  nil
  (interpret-patch-result [_ ctx] ctx))

(deftype PatchMethod [])

(extend-protocol m/Method
  PatchMethod
  (keyword-binding [_] :patch)
  (safe? [_] false)
  (idempotent? [_] false)
  (request [_ ctx]
    (d/chain
     (when-let [f (or (get-in ctx [:resource :methods (:method ctx) :response])
                      (get-in ctx [:resource :methods :* :response]))]
       (m/apply-response-fn f ctx))
     (fn [res]
       (interpret-patch-result res ctx))
     (fn [ctx]
       (let [status (get-in ctx [:response :status])]
         (cond-> ctx
           (not status) (assoc-in [:response :status] 200)
           (not (-> ctx :response :body)) zero-content-length))))))
